import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import me.lemire.integercompression.Composition;
import me.lemire.integercompression.FastPFOR;
import me.lemire.integercompression.IntWrapper;
import me.lemire.integercompression.IntegerCODEC;
import me.lemire.integercompression.VariableByte;
import me.lemire.integercompression.differential.IntegratedBinaryPacking;
import me.lemire.integercompression.differential.IntegratedComposition;
import me.lemire.integercompression.differential.IntegratedIntegerCODEC;
import me.lemire.integercompression.differential.IntegratedVariableByte;


if (ctx._source.buy != null){
		
	// this is the existing compressed buy array, being decompressed.
	int[] barr = get_decompressed_array_from_compressed_array(
			ctx._source.buy, ctx._source.decompressed_length);
	
	//now we prepare the merged array.
	sparse_arr = prepare_merged_array(sparse_arr, barr);

}


int[] sorted_compressible_buy_array = get_arr(sparse_arr,0,null);
int[] sorted_compressible_sell_array = get_arr(sparse_arr,1,null);
ctx._source.buy = get_compressed_int_array(sorted_compressible_buy_array);
ctx._source.sell = get_compressed_int_array(sorted_compressible_sell_array);
ctx._source.decompressed_length = sorted_compressible_buy_array.length;


/****
	 * TESTED TAKES THE GIVEN UNCOMPRESSED ARRAY AND COMPRESSES IT DEPENDING ON
	 * ITS TYPE.
	 * 
	 * 
	 * @param data
	 * @param type
	 *            : 0 uncompressed, 1 compressed
	 * @return
	 */
	public static int[] get_compressed_int_array(int[] data) {
		
		IntegratedIntegerCODEC codec = new IntegratedComposition(
				new IntegratedBinaryPacking(), new IntegratedVariableByte());

		int[] compressed = new int[data.length + 1024];

		IntWrapper inputoffset = new IntWrapper(0);
		IntWrapper outputoffset = new IntWrapper(0);
		codec.compress(data, inputoffset, data.length, compressed,
				outputoffset);

		// System.out.println("compressed from " + data.length * 4 / 1024
		// + "KB to " + outputoffset.intValue() * 4 / 1024 + "KB");

		compressed = Arrays.copyOf(compressed, outputoffset.intValue());
		// System.out.println("invoked compression of sorted array:");
		return compressed;
		
	}

/****
	 * given a compressed array, and its type,(whether unsorted or sorted)
	 * returns a decompressed array.
	 * 
	 * 
	 * @param compressed_arr
	 * @param expected_length_of_decompressed_array
	 * @param type
	 * @return
	 */
	public static int[] get_decompressed_array_from_compressed_array(
			ArrayList<Integer> compressed_list, int expected_length_of_decompressed_array) {

		int[] compressed_arr = new int[compressed_list.size()];
		
		for(int i=0; i < compressed_list.size(); i++){
			compressed_arr[i] = compressed_list.get(i);
		}

		int[] recovered = new int[expected_length_of_decompressed_array + 1];

		
		// sorted
		IntegratedIntegerCODEC codec = new IntegratedComposition(
				new IntegratedBinaryPacking(), new IntegratedVariableByte());
		recovered = new int[expected_length_of_decompressed_array];
		IntWrapper recoffset = new IntWrapper(0);
		codec.uncompress(compressed_arr, new IntWrapper(0),
				compressed_arr.length, recovered, recoffset);
		// System.out
		// .println("invoking decompression of sorted array and returned \n");
		
		return recovered;

	}


/****
	first unique the sorted_compressible arr
	then addall the sparse_arr into it.
	then unique it and sort it.
	then return the merged sparse_arr
	@return[Array] : the merged array, sorted.
****/
public static int[] prepare_merged_array(int[] sparse_arr, int[] sorted_compressible_arr){

	//println "came to prepare merged array with sparse arr";
	//print_arr(sparse_arr);

	//println "sorted compressible arr";
	//print_arr(sorted_compressible_arr);


	ArrayList<Integer> original_compressible_arr = new ArrayList<Integer>(Arrays.asList(sorted_compressible_arr));

	//println "original compressible arr uniqued";
	//println(original_compressible_arr);

	// now for each of the things in the sparse 
	// we want to add all , unique and sort.
	original_compressible_arr.addAll(sparse_arr);

	//println "after add all"
	//println(original_compressible_arr);

	return original_compressible_arr.unique().sort();

}


public static int[] get_arr(int[] sparse_arr, Integer buy_or_sell,
			int[] sorted_compressible_arr) {

		// best option is to first prepare the merged array
		// then fill it.

		// this assumes that the new sparse arr is going to be of higher digits.
		// the real idea is to first create a properly merged array.
		// we might upload anything in the newer array.
		int max_day_id = sparse_arr[sparse_arr.length - 1];
		
		sorted_compressible_arr = new int[max_day_id + 1];
		 
		// the way i would do it is this ->
		// get the first index, and the next index.
		// for every day id.
		for(int index = 0; index < sparse_arr.length; index++){
			
			int current_element = sparse_arr[index];
			
			// fill all indices, less than the first element with first_element if the first element is not zero.
			// eg if the array itself starts with 3, then fill 0,1,2, with 3.
			if(index == 0 && current_element > 0){
				for(int prefill = 0; prefill < current_element; prefill++){
					sorted_compressible_arr[prefill] = current_element;
				}
			}

			// the current element has to be made equal to itself.
			// i.e : 4 -> 4.			
			sorted_compressible_arr[current_element] = current_element;

			// now get the next element.
			// as long as it exists.
			// then for the buy array, fill all intervening indices with the current_element.
			// for the sell array, fill all intervening indices with the next element.
			if((index + 1) < sparse_arr.length)
			{
				int next_element = sparse_arr[index + 1];
				for(int fill_with = current_element + 1;fill_with < next_element; fill_with++){
					
					if(buy_or_sell.equals(0)){
						sorted_compressible_arr[fill_with] = current_element;
					}
					else{
						sorted_compressible_arr[fill_with] = next_element;
					}

				}
			}
		}

		//println "the sorted compressible array is:"
		//println sorted_compressible_arr;

		return sorted_compressible_arr;
	}

public static void print_arr(arr) {
	for(int i = 0; i < arr.length; i ++){
		println "index: " + i + " value: " + arr[i];
	}
}
